{
  description = "Nixos config flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    niri = {
      url = "github:sodiboo/niri-flake";    
      inputs.nixpkgs.follows = "nixpkgs";
    };

    anyrun = {
      url = "github:anyrun-org/anyrun";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    zen-browser = {
      url = "github:0xc000022070/zen-browser-flake";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # Neovim plugins
    lz-n = {
      url = "github:nvim-neorocks/lz.n";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # Base16.nix flake and templates
    base16.url = "github:SenchoPens/base16.nix";

    tt-schemes = {
      url = "github:tinted-theming/schemes";
      flake = false;
    };

    base16-kitty = {
      url = "github:kdrag0n/base16-kitty";
      flake = false;
    };

    base16-waybar = {
      url = "github:tinted-theming/base16-waybar";
      flake = false;
    };

  };

  outputs = { self, nixpkgs, ... }@inputs: {

    nixosConfigurations.quote = nixpkgs.lib.nixosSystem {
      specialArgs = {inherit inputs;};
      modules = [
        ./hosts/quote/default.nix
        inputs.home-manager.nixosModules.default
        inputs.niri.nixosModules.niri
        inputs.base16.nixosModule
      ];
    };

    nixosConfigurations.curly = nixpkgs.lib.nixosSystem {
      specialArgs = {inherit inputs;};
      modules = [
        ./hosts/curly/default.nix
        inputs.home-manager.nixosModules.default
        inputs.niri.nixosModules.niri
        inputs.base16.nixosModule
      ];
    };

  };
}
