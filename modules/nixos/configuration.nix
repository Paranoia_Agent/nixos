{ pkgs, inputs, ... }:

{
  # Set your time zone.
  time.timeZone = "America/Sao_Paulo";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "pt_BR.UTF-8";
    LC_IDENTIFICATION = "pt_BR.UTF-8";
    LC_MEASUREMENT = "pt_BR.UTF-8";
    LC_MONETARY = "pt_BR.UTF-8";
    LC_NAME = "pt_BR.UTF-8";
    LC_NUMERIC = "pt_BR.UTF-8";
    LC_PAPER = "pt_BR.UTF-8";
    LC_TELEPHONE = "pt_BR.UTF-8";
    LC_TIME = "pt_BR.UTF-8";
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable printing autodiscovery.
  services.avahi = {
    enable = true;
    nssmdns4 = true;
    openFirewall = true;
  };

  # Enable sound with pipewire.
  services.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };

  # Enable XDG deskpop integration
  # xdg.portal = {
  #   enable = true;
  #   extraPortals = with pkgs; [
  #     xdg-desktop-portal-gnome
      # xdg-desktop-portal-gtk
  #   ];
  # };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.paranoia-agent = {
    isNormalUser = true;
    description = "Anthony Bonfim";
    extraGroups = [ "networkmanager" "wheel" ];
    shell = pkgs.zsh;
  };

  # Environment Variables
  environment = {
    variables = {
      EDITOR = "nvim";
      SUDO_EDITOR = "nvim";
    };
    sessionVariables = rec {
      NIXOS_OZONE_WL = "1";
      XDG_CACHE_HOME  = "$HOME/.cache";
      XDG_CONFIG_HOME = "$HOME/.config";
      XDG_DATA_HOME   = "$HOME/.local/share";
      XDG_STATE_HOME  = "$HOME/.local/state";
      XDG_BIN_HOME    = "$HOME/.local/bin";
      PATH = [ 
        "${XDG_BIN_HOME}"
      ]; 
    };
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # System Packages
  environment.systemPackages = with pkgs; [
    vim
    wget
    git
    nvd
    nix-output-monitor
    gparted
    # zen-browser
    inputs.zen-browser.packages."${system}".default
  ];

  programs = {
    # Required to enable zsh system-wide
    zsh.enable = true;
    nh = {
      enable = true;
      flake = "/home/paranoia-agent/Flakes";
    };
  };

  # Run a timer to sync obsidian vault with google drive every 15 minutes
  systemd.timers."bisync" = {
    wantedBy = [ "timers.target" ];
    timerConfig = {
      OnBootSec = "15m";
      OnUnitActiveSec = "15m";
      Unit = "bisync.service";
    };
  };
  systemd.services."bisync" = {
    serviceConfig = {
      Type = "oneshot";
      User = "paranoia-agent";
    };
    path = with pkgs; [ rclone ];
    script = ''
      rclone bisync ~/Documents/Obsidian gdrive:Obsidian/ --create-empty-src-dirs
      # rclone bisync ~/Documents/Obsidian gdrive:Obsidian/ --create-empty-src-dirs --resync
    '';
  };

  # Enable geoclue daemon for gammastep.
  services.geoclue2.enable = true;

  # Enable flatpak.
  services.flatpak.enable = true;

  # Sunshine Game Streaming
  services.sunshine = {
    enable = true;
    package = pkgs.sunshine;
    autoStart = true;
    capSysAdmin = true;
    openFirewall = true;
  };

  # Piper service
  services.ratbagd.enable = true;

  # Enable binary cache for anyrun launcher packages.
  nix.settings = {
    builders-use-substitutes = true;
    substituters = [
      "https://anyrun.cachix.org"
    ];
    extra-trusted-public-keys = [
      "anyrun.cachix.org-1:pqBobmOjI7nKlsUMV25u9QHa9btJK65/C8vnO3p346s="
    ];
  };

  # Overlays
  nixpkgs.overlays = with inputs; [
    niri.overlays.niri
  ];

  # Enable Nix Flakes
  nix.settings.experimental-features = [ "nix-command" "flakes" ];
}
