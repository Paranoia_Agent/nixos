{ pkgs, ... }:

{
  # Enable X server and Gnome Desktop Environment
  services.xserver = {
    enable = true;
    displayManager.gdm.enable = true;
    desktopManager.gnome.enable = true;
  };

  # Disable Gnome Default Packages
   environment.gnome.excludePackages = with pkgs; [
     gnome-photos
     gnome-console    
     gnome-tour
     cheese # webcam tool
     epiphany # web browser
     geary # email reader
     gnome-terminal
     totem # video player
     atomix # puzzle game
     # evince # document viewer
     # gedit # text editor
     # gnome-characters
     gnome-music
     hitori # sudoku game
     iagno # go game
     tali # poker game
   ];
}
