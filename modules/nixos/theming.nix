{ config, pkgs, inputs, ... }:

{
  home-manager.users.paranoia-agent = {

    # Set GTK theme and icons.
    gtk = {
      enable = true;
      theme = {
        name = "adw-gtk3-dark";
        package = pkgs.adw-gtk3;
      };
      iconTheme = {
        name = "Papirus-Dark";
        package = pkgs.papirus-icon-theme.override {
          color = "adwaita";
        };
      };
    };

    # Set QT theme.
    qt = {
      enable = true;
      platformTheme.name = "adwaita";
      style = {
        name = "adwaita-dark";
        package = pkgs.adwaita-qt;
      };
    };

    # Set cursor theme.
    home.pointerCursor = {
      x11.enable = true;
      gtk.enable = true;

      # name = "Bibata-Modern-Classic";
      # package = pkgs.bibata-cursors;
      name = "Adwaita";
      package = pkgs.adwaita-icon-theme;
      size = 24;
    };

    # Niri
    programs.niri.settings.layout = 
    with config.scheme;
    let
      inherit base02 base0D;
    in
    {
      focus-ring = {
        active.color = "${base0D}";
        inactive.color = "${base02}";
      };
      border = {
        active.color = "${base0D}";
        inactive.color = "${base02}";
      };
    };

    # Kitty
    programs.kitty.extraConfig = 
    let
      schemeFile = config.scheme inputs.base16-kitty;
    in
    ''
      include ${schemeFile}
    '';

    # Waybar
    programs.waybar = {
      style = 
      let
        schemeFile = config.scheme inputs.base16-waybar;
      in
      ''
        @import "${schemeFile}";
      '';

      settings.mainBar."clock".calendar.format = 
      with config.scheme.withHashtag;
      let
        inherit base03 base05 base0D;
      in
      {
        months = "<span color='${base05}'><b>{}</b></span>";
        days = "<span color='${base05}'><b>{}</b></span>";
        weekdays = "<span color='${base03}'><b>{}</b></span>";
        today = "<span color='${base0D}'><b><u>{}</u></b></span>";
      };
    };

    # Swaync
    services.swaync.style = 
    let
      schemeFile = config.scheme inputs.base16-waybar;
    in
    ''
      @import "${schemeFile}";
    '';

    # Swaylock Effects
    home.file.".config/swaylock/config".text =
    with config.scheme;
    let
      inherit base05 base09 base0B base0D;
    in
    ''
      bs-hl-color=${base0D}
      key-hl-color=${base0B}

      caps-lock-bs-hl-color=${base0D}
      caps-lock-key-hl-color=${base0B}

      ring-color=${base05}
      text-color=${base05}

      ring-clear-color=${base0D}
      text-clear-color=${base0D}

      ring-caps-lock-color=${base09}
      text-caps-lock-color=${base09}

      ring-ver-color=${base0E}
      text-ver-color=${base0E}

      ring-wrong-color=${base08}
      text-wrong-color=${base08}
    '';
  };
}
