{
  home.file = {
    ".config/swaylock/config" = {
      text = ''
        font=Inter Variable
        screenshots
        clock
        indicator
        indicator-radius=100
        indicator-thickness=8
        timestr=%R
        datestr=%d/%m/%Y
        effect-blur=7x5
        effect-vignette=0.3:0.3
        fade-in=0.2
        
        inside-color=00000000
        inside-clear-color=00000000
        inside-caps-lock-color=00000000
        inside-ver-color=00000000
        inside-wrong-color=00000000
        
        line-color=00000000
        line-clear-color=00000000
        line-caps-lock-color=00000000
        line-ver-color=00000000
        line-wrong-color=00000000
        
        separator-color=00000000
      '';
    };
  };
}
