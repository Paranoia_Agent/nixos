{
  services.gammastep = {
    enable = false;
    dawnTime = "06:00";
    duskTime = "18:00";
    provider = "geoclue2";
    temperature = {
      day = 5500;
      night = 3700;
    };
  };
}
