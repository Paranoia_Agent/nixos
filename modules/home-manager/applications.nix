{ pkgs, ...}:

{
  home.packages = with pkgs; [
    # Desktop applications
    blueman
    davinci-resolve
    celluloid
    firefox
    gamemode
    gamescope
    helvum
    heroic
    inkscape
    krita
    mangohud
    mpv
    obsidian
    obs-studio
    pdfarranger
    piper
    protonup-qt
    qbittorrent
    scrcpy
    scribus
    steam
    vesktop

    # Terminal applications
    steam-run
    pfetch-rs
    rclone
    coreutils
    clang
    fd
    btop
    yt-dlp
    gnumake
    unzip
    ffmpeg
    xorg.libxcvt
    cmake

    # Programming Languages
    lua
    python3

    # Xwayland
    xwayland
    xwayland-satellite

    # Gnome
    gnome-extension-manager
    dconf-editor
    gnome-tweaks

    # Gnome extensions
    gnomeExtensions.hot-edge
    gnomeExtensions.just-perfection
    gnomeExtensions.tiling-assistant
    gnomeExtensions.caffeine
    gnomeExtensions.alphabetical-app-grid
    gnomeExtensions.app-hider
  ];

}

