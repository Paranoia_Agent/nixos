{
  # Enable ZSH
  programs.zsh = {
    enable = true;
    enableCompletion = true;
    autosuggestion.enable = true;
    syntaxHighlighting.enable = true;

    initExtra = ''
      bindkey '^f' autosuggest-accept
    '';
  };

  # Starship Prompt Options
  programs.starship = {
    enable = true;
    enableZshIntegration = true;

    settings = {
      add_newline = false;
      line_break = {
        disabled = true;
      };
      battery = {
        disabled = true;
      };
    };
  };
}
