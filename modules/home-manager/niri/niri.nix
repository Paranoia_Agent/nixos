{ config, pkgs, ... }:

{
  imports = [
    ../waybar.nix
    ../anyrun.nix
    ../swaylock.nix
    ../swaync.nix
    ../gammastep.nix
  ];
  
  home.packages = with pkgs; [
    # Settings
    pulseaudio
    pavucontrol
    brightnessctl
    # Lockscreen
    swayidle
    swaylock-effects
    # Wallpaper
    swww
  ];

  programs.niri.settings = {
    
    hotkey-overlay.skip-at-startup = true;

    # Default screenshot path.
    screenshot-path = "~/Pictures/Screenshots/Screenshot from %Y-%m-%d %H-%M-%S.png";

    # Hide cursor after 30 seconds
    cursor.hide-after-inactive-ms = 30000;

    # Enable mouse warp to center of focused window;
    input.focus-follows-mouse.enable = false;
    input.warp-mouse-to-focus = false;

    # Mouse acceleration
    input.mouse.accel-speed = 0.0;

    # Touchpad config
    input.touchpad = {
      tap = true;
      dwt = true;
      natural-scroll = true;
      scroll-method = "two-finger";
    };

    # Fixes freeze on fullscreen xwayland windows
    debug = {
      disable-direct-scanout = [];
    };

    layout = {
      gaps = 12;
      center-focused-column = "never";
      default-column-width = { proportion = 1.0 / 2.0; };
      preset-column-widths = [
        { proportion = 1.0 / 3.0; }
        { proportion = 1.0 / 2.0; }
        { proportion = 2.0 / 3.0; }
      ];

      struts = {
        left = 1;
        right = 1;
      };

      # Focus Ring (enabled by default).
      focus-ring = {
        enable = true;
        width = 4;
      };

      # Border (disabled by default).
      border = {
        enable = false;
        width = 4;
      };

    };

    # Animation options.
    animations = {
      enable = true;
      slowdown = 1.0;
    };

    # Compositor decorations.
    prefer-no-csd = true;

    # Startup applications.
    spawn-at-startup = [
      { 
        command = [
          "swayidle" "-w"
          "timeout" "600" ''niri msg action power-off-monitors'' 
          "timeout" "900" "swaylock" 
        ]; 
      }
      { command = ["swww-daemon"]; }
      { command = ["waybar"]; }
      { command = ["swaync"]; }
      { command = ["xwayland-satellite"];}
    ];

    environment = {
      DISPLAY = ":0";
    };
    
    # Window rules.
    window-rules = [
      {
        geometry-corner-radius = let r = 12.0; in {
          top-left = r;
          top-right = r;
          bottom-left = r;
          bottom-right = r;
        };
        clip-to-geometry = true;
      }
    ];
    
    binds = with config.lib.niri.actions; {
    
      # Show hotkey list.
      "Mod+Shift+Slash".action                = show-hotkey-overlay;

      # Program binds.
      "Mod+Alt+L".action                      = spawn "swaylock"; 
      "Mod+Space".action                      = spawn "anyrun";   
      "Mod+Return".action                     = spawn "kitty";    

      # Volume key binds.
      "XF86AudioRaiseVolume".action           = spawn "wpctl" "set-volume" "@DEFAULT_AUDIO_SINK@" "0.1+";
      "XF86AudioLowerVolume".action           = spawn "wpctl" "set-volume" "@DEFAULT_AUDIO_SINK@" "0.1-";
      "XF86AudioMute".action                  = spawn "wpctl" "set-volume" "@DEFAULT_AUDIO_SINK@" "toggle";
      "XF86AudioMicMute".action               = spawn "wpctl" "set-volume" "@DEFAULT_AUDIO_SINK@" "toggle";

      "Mod+Q".action                          = close-window;

      # Mapping for arrow keys and vim-like bidings on colemak-dh.
      "Mod+Left".action                       = focus-column-left;
      "Mod+Down".action                       = focus-window-down;
      "Mod+Up".action                         = focus-window-up;
      "Mod+Right".action                      = focus-column-right;
      "Mod+M".action                          = focus-column-left;
      "Mod+N".action                          = focus-window-down;
      "Mod+E".action                          = focus-window-up;
      "Mod+I".action                          = focus-column-right;
      
      "Mod+Ctrl+Left".action                  = move-column-left;
      "Mod+Ctrl+Down".action                  = move-window-down;
      "Mod+Ctrl+Up".action                    = move-window-up;
      "Mod+Ctrl+Right".action                 = move-column-right;
      "Mod+Ctrl+M".action                     = move-column-left;
      "Mod+Ctrl+N".action                     = move-window-down;
      "Mod+Ctrl+E".action                     = move-window-up;
      "Mod+Ctrl+I".action                     = move-column-right;

      "Mod+Home".action                       = focus-column-first;
      "Mod+End".action                        = focus-column-last;
      "Mod+Ctrl+Home".action                  = move-column-to-first;
      "Mod+Ctrl+End".action                   = move-column-to-last;

      "Mod+Shift+Left".action                 = focus-monitor-left;
      "Mod+Shift+Down".action                 = focus-monitor-down;
      "Mod+Shift+Up".action                   = focus-monitor-up;
      "Mod+Shift+Right".action                = focus-monitor-right;
      "Mod+Shift+M".action                    = focus-monitor-left;
      "Mod+Shift+N".action                    = focus-monitor-down;
      "Mod+Shift+E".action                    = focus-monitor-up;
      "Mod+Shift+I".action                    = focus-monitor-right;
      
      "Mod+Shift+Ctrl+Left".action            = move-column-to-monitor-left;
      "Mod+Shift+Ctrl+Down".action            = move-column-to-monitor-down;
      "Mod+Shift+Ctrl+Up".action              = move-column-to-monitor-up;
      "Mod+Shift+Ctrl+Right".action           = move-column-to-monitor-right;
      "Mod+Shift+Ctrl+M".action               = move-column-to-monitor-left;
      "Mod+Shift+Ctrl+N".action               = move-column-to-monitor-down;
      "Mod+Shift+Ctrl+E".action               = move-column-to-monitor-up;
      "Mod+Shift+Ctrl+I".action               = move-column-to-monitor-right;

      "Mod+Page_Down".action                  = focus-workspace-down;
      "Mod+Page_Up".action                    = focus-workspace-up;
      "Mod+L".action                          = focus-workspace-down;
      "Mod+U".action                          = focus-workspace-up;
      "Mod+Ctrl+Page_Down".action             = move-column-to-workspace-down;
      "Mod+Ctrl+Page_Up".action               = move-column-to-workspace-up;
      "Mod+Ctrl+L".action                     = move-column-to-workspace-down;
      "Mod+Ctrl+U".action                     = move-column-to-workspace-up;

      "Mod+Shift+Page_Down".action            = move-workspace-down;
      "Mod+Shift+Page_Up".action              = move-workspace-up;
      "Mod+Shift+L".action                    = move-workspace-down;
      "Mod+Shift+U".action                    = move-workspace-up;

      "Mod+WheelScrollDown".action            = focus-workspace-down;
      "Mod+WheelScrollUp".action              = focus-workspace-up;
      "Mod+Ctrl+WheelScrollDown".action       = move-column-to-workspace-down;
      "Mod+Ctrl+WheelScrollUp".action         = move-column-to-workspace-up;
      
      "Mod+WheelScrollRight".action           = focus-column-right;
      "Mod+WheelScrollLeft".action            = focus-column-left;
      "Mod+Ctrl+WheelScrollRight".action      = move-column-right;
      "Mod+Ctrl+WheelScrollLeft".action       = move-column-left;
      
      "Mod+Shift+WheelScrollDown".action      = focus-column-right;
      "Mod+Shift+WheelScrollUp".action        = focus-column-left;
      "Mod+Ctrl+Shift+WheelScrollDown".action = move-column-right;
      "Mod+Ctrl+Shift+WheelScrollUp".action   = move-column-left;

      "Mod+1".action                          = focus-workspace 1;
      "Mod+2".action                          = focus-workspace 2;
      "Mod+3".action                          = focus-workspace 3;
      "Mod+4".action                          = focus-workspace 4;
      "Mod+5".action                          = focus-workspace 5;
      "Mod+6".action                          = focus-workspace 6;
      "Mod+7".action                          = focus-workspace 7;
      "Mod+8".action                          = focus-workspace 8;
      "Mod+9".action                          = focus-workspace 9;

      "Mod+Ctrl+1".action                     = move-column-to-workspace 1;
      "Mod+Ctrl+2".action                     = move-column-to-workspace 2;
      "Mod+Ctrl+3".action                     = move-column-to-workspace 3;
      "Mod+Ctrl+4".action                     = move-column-to-workspace 4;
      "Mod+Ctrl+5".action                     = move-column-to-workspace 5;
      "Mod+Ctrl+6".action                     = move-column-to-workspace 6;
      "Mod+Ctrl+7".action                     = move-column-to-workspace 7;
      "Mod+Ctrl+8".action                     = move-column-to-workspace 8;
      "Mod+Ctrl+9".action                     = move-column-to-workspace 9;

      "Mod+Comma".action                      = consume-window-into-column;
      "Mod+Period".action                     = expel-window-from-column;

      "Mod+BracketLeft".action                = consume-or-expel-window-left;
      "Mod+BracketRight".action               = consume-or-expel-window-right;

      "Mod+R".action                          = switch-preset-column-width;
      "Mod+Shift+R".action                    = reset-window-height;
      "Mod+F".action                          = maximize-column;
      "Mod+Shift+F".action                    = fullscreen-window;
      "Mod+C".action                          = center-column;

      "Mod+Minus".action                      = set-column-width  "-10%";
      "Mod+Equal".action                      = set-column-width  "+10%";
      "Mod+Shift+Minus".action                = set-window-height "-10%";
      "Mod+Shift+Equal".action                = set-window-height "+10%";

      "Print".action                          = screenshot;
      "Ctrl+Print".action                     = screenshot-screen;
      "Alt+Print".action                      = screenshot-window;

      "Mod+Shift+Q".action                    = quit;
      "Mod+Shift+P".action                    = power-off-monitors;
    };
  };
}
