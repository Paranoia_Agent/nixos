{
  programs.niri.settings = {

    # Monitor resolition.
    outputs."HDMI-A-1" = {
      scale = 1.0;
      mode = {
        width = 1920;
        height = 1080;
        refresh = 60.0;
      };
    };

    # Keyboard layout options
    input.keyboard.xkb = {
      layout = "us, br";
      variant = "colemak_dh_ortho,";
      options = "grp:menu_toggle";
    };

  };
}
