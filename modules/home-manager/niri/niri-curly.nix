{
  programs.niri.settings = {

    # Resolution scale.
    outputs."eDP-1".scale = 2.0;

    # Keyboard layout options
    input.keyboard.xkb = {
      layout = "us, br";
      variant = "colemak_dh_iso,";
      options = "caps:swapescape, altwin:swap_lalt_lwin, grp:menu_toggle";
    };

   # Touchpad options
    input.touchpad = {
      tap = true;
      dwt = false;
      natural-scroll = true;
      click-method = "button-areas";
    };

  };
}
