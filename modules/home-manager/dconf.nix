{
  dconf = {
    enable = true;
    settings = {
      "org/gnome/desktop/interface" = {
        color-scheme = "prefer-dark";
        cursor-size=24;
        # cursor-theme="Bibata-Modern-Classic";
        cursor-theme = "Adwaita";
        document-font-name="Inter Variable 11";
        enable-animations=true;
        font-antialiasing="grayscale";
        font-hinting="slight";
        font-name="Inter Variable 11";
        gtk-theme="adw-gtk3-dark";
        icon-theme="Papirus-Dark";
        # monospace-font-name="CaskaydiaCove Nerd Font Mono 11";
        monospace-font-name="MapleMono NF 11";
      };
  
      "org/gnome/shell/extensions/caffeine" = {
        indicator-position-max=2;
        restore-state=true;
      };
  
      "org/gnome/shell/extensions/just-perfection" = {
        animation=4;
        dash-icon-size=48;
        dash-separator=false;
        events-button=false;
        keyboard-layout=false;
        panel=true;
        panel-in-overview=true;
        panel-notification-icon=true;
        ripple-box=false;
        search=false;
        window-picker-icon=false;
        window-preview-caption=false;
        workspace-switcher-should-show=true;
        world-clock=false;
      };
    };
  };
}
