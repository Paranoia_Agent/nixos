{
  services.swaync.enable = true;
  services.swaync.settings = {
    positionX = "right";
    positionY = "top";
    control-center-margin-top = 6;
    control-center-margin-bottom= 6;
    control-center-margin-left= 6;
    control-center-margin-right= 6;
    control-center-width = 450;
    control-center-height = 600;
    fit-to-screen = true;
    relative-timestamps = true;
    
    layer-shell = true;
    layer = "overlay";
    control-center-layer = "top";
    cssPriority = "application";
    notification-icon-size = 50;
    notification-body-image-height = 50;
    notification-body-image-width = 200;
    notification-window-width = 400;
    notification-inline-replies = true;
    notification-2fa-action = true;
    timeout = 10;
    timeout-low = 5;
    timeout-critical = 0;
    transition-time = 200;
    image-visibility = "when-available";
    hide-on-clear = true;
    hide-on-action = true;
    keyboard-shortcuts = true;
    script-fail-notify = true;

    widget-config = {
      title = {
        text = "Notifications";
        button-text = "󰎟 Clear";
        "clear-all-button" = true;
      };
      dnd = {
        text = "Do not disturb";
      };
      label = {
        max-lines = 5;
        text = "Label Text";
      };
      mpris = {
        image-size = 75;
        image-radius = 6;
        blur = false;
      };
      volume = {
        label = "";
        show-per-app = true;
        show-per-app-icon = true;
        show-per-app-label = true;
      };
      backlight = {
        label = "󰃠";
        device = "acpi_video0";
      };
      buttons-grid = {
        actions = [
          {
            active =false;
            command = "niri msg action screenshot";
            label = "󰄀";
            type = "button";
          }
          {
            active = false;
            command = "swaylock";
            label = "󰌾";
            type = "button";
          }
          {
            active = false;
            command = "niri msg action quit --skip-confirmation";
            label = "󰗽";
            type = "button";
          }
          {
            active = false;
            command = "reboot";
            label = "󰜉";
            type = "button";
          }
          {
            active = false;
            command = "shutdown now";
            label = "󰐥";
            type = "button";
          }
        ];
      };
    };
    widgets = [
      "title"
      "buttons-grid"
      "volume"
      "backlight"
      "mpris"
      "notifications"
      "dnd"
    ];
  };
  services.swaync.style = ''
    * {
      all: unset;
      font-size: 14px;
      font-family: "Inter Variable", "Symbols Nerd Font Mono";
      transition: 200ms;
    }

    trough highlight {
      background: @base05;
    }

    scale trough {
      background-color: @base01;
      margin: 8px;
      min-height: 8px;
      min-width: 70px;
    }

    slider {
      background-color: @base0D;
    }

    .floating-notifications.background .notification-row .notification-background {
      /* box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.8), inset 0 0 0 1px @base02; */
      border: 1px solid @base02;
      box-shadow: none;
      border-radius: 12px;
      margin: 8px;
      padding: 0px;
      background-color: @base01;
      color: @base05;
    }

    .floating-notifications.background .notification-row .notification-background .notification {
      padding: 8px;
      border-radius: 12px;
    }

    .floating-notifications.background .notification-row .notification-background .notification.critical {
      box-shadow: inset 0 0 0 5px @base08;
    }

    .floating-notifications.background .notification-row .notification-background .notification .notification-content {
      margin: 8px;
    }

    .floating-notifications.background .notification-row .notification-background .notification .notification-content .summary {
      color: @base03;
    }

    .floating-notifications.background .notification-row .notification-background .notification .notification-content .time {
      color: @base03;
    }

    .floating-notifications.background .notification-row .notification-background .notification .notification-content .body {
      color: @base05;
    }

    .floating-notifications.background .notification-row .notification-background .notification > *:last-child > * {
      min-height: 3.5em;
    }

    .floating-notifications.background .notification-row .notification-background .notification > *:last-child > * .notification-action {
      /* box-shadow: inset 0 0 0 1px @base02; */
      box-shadow: none;
      background-color: @base01;
      color: @base05;
      margin: 8px;
      border-radius: 12px;
    }

    .floating-notifications.background .notification-row .notification-background .notification > *:last-child > * .notification-action:hover {
      /* box-shadow: inset 0 0 0 1px @base02; */
      box-shadow: none;
      background-color: @base01;
      color: @base05;
    }

    .floating-notifications.background .notification-row .notification-background .notification > *:last-child > * .notification-action:active {
      /* box-shadow: inset 0 0 0 1px @base02; */
      box-shadow: none;
      background-color: @base0B;
      color: @base05;
    }

    .floating-notifications.background .notification-row .notification-background .close-button {
      margin: 8px;
      padding: 2px;
      border-radius: 12px;
      color: @base04;
      background-color: @base02;
    }

    .floating-notifications.background .notification-row .notification-background .close-button:hover {
      background-color: @base03;
      color: @base04;
    }

    .floating-notifications.background .notification-row .notification-background .close-button:active {
      background-color: @base04;
      color: @base04;
    }

    .control-center {
      /* box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.8), inset 0 0 0 1px @base01; */
      border: 1px solid @base02;
      box-shadow: none;
      border-radius: 12px;
      margin: 2px;
      background-color: @base00;
      color: @base05;
      padding: 15px;
    }

    .control-center-list-placeholder {
      color: @base01;
    }

    .control-center .notification-row .notification-background {
      border-radius: 12px;
      color: @base05;
      background-color: @base01;
      /* box-shadow: inset 0 0 0 1px @base02; */
      box-shadow: none;
      margin: 9px;
    }

    .control-center .notification-row .notification-background .notification {
      padding: 8px;
      border-radius: 12px;
    }

    .control-center .notification-row .notification-background .notification.critical {
      box-shadow: inset 0 0 0 5px @base08;
    }

    .control-center .notification-row .notification-background .notification .notification-content {
      margin: 8px;
    }

    .control-center .notification-row .notification-background .notification .notification-content .summary {
      color: @base03;
    }

    .control-center .notification-row .notification-background .notification .notification-content .time {
      color: @base03;
    }

    .control-center .notification-row .notification-background .notification .notification-content .body {
      color: @base05;
    }

    .control-center .notification-row .notification-background .notification > *:last-child > * {
      min-height: 3.5em;
    }

    .control-center .notification-row .notification-background .notification > *:last-child > * .notification-action {
      /* box-shadow: inset 0 0 0 1px @base02; */
      box-shadow: none;
      border-radius: 12px;
      color: @base05;
      background-color: @base00;
      margin: 8px;
    }

    .control-center .notification-row .notification-background .notification > *:last-child > * .notification-action:hover {
      /* box-shadow: inset 0 0 0 1px @base02; */
      box-shadow: none;
      background-color: @base01;
      color: @base05;
    }

    .control-center .notification-row .notification-background .notification > *:last-child > * .notification-action:active {
      /* box-shadow: inset 0 0 0 1px @base02; */
      box-shadow: none;
      background-color: @base0B;
      color: @base05;
    }

    .control-center .notification-row .notification-background .close-button {
      margin: 8px;
      padding: 2px;
      border-radius: 12px;
      color: @base04;
      background-color: @base02;
    }

    .close-button {
      border-radius: 6px;
    }

    .control-center .notification-row .notification-background .close-button:hover {
      background-color: @base03;
      color: @base04;
    }

    .control-center .notification-row .notification-background .close-button:active {
      background-color: @base04;
      color: @base04;
    }

    .control-center-dnd {
      background: @base01;
      border: 1px solid @base02;
    }

    .control-center-dnd:checked {
      background: @base01;
    }

    .control-center-dnd slider {
      background: @base02;
    }

    .notification-group-headers {
      color: @base05;
      margin-left: 9px;
    }

    .notification-group-buttons {
      color: @base05;
      margin-right: 9px;
    }

    .notification-group-buttons button {
      padding: 5px;
      border-radius: 12px;
      color: @base05;
      background-color: @base01;
    }
    
    .notification-group-buttons button:hover {
      color: @base05;
      background-color: @base02;
    }
    
    .notification-group-buttons button:active {
      color: @base05;
      background-color: @base03;
    }

    .image {
      padding-right: 1.0rem;
      border-radius: 8px;
    }

    /* Title */

    .widget-title {
      margin: 8px;
    }

    .widget-title button {
      padding: 10px;
      border-radius: 12px;
      background-color: @base01;
    }

    .widget-title label {
      font-size: 1.3em;
    }

    .widget-title button label {
      font-size: 1.0em;
    }

    .widget-title button:hover {
      background-color: @base02;
    }

    .widget-title button:active {
      background-color: @base03;
    }

    /* DND */

    .widget-dnd {
      margin: 8px;
    }

    .widget-dnd > switch {
      padding: 2px;
      border-radius: 10px;
    }

    .widget-dnd > switch slider {
      padding: 2px;
      border-radius: 8px;
      background-color: @base0D;
    }

    /* Buttons */

    .widget-buttons-grid {
      padding: 8px;
      margin: 8px;
      border-radius: 12px;
      background-color: @base01;
    }

    .widget-buttons-grid > flowbox > flowboxchild > button {
      border:none;
      border-radius: 12px;
      background-color: unset;
      min-width: 75px;
      min-height: 50px;
    }

    .widget-buttons-grid > flowbox > flowboxchild > button label {
      font-size: 16px;
    }

    .widget-buttons-grid > flowbox > flowboxchild > button:hover {
      background-color: @base02;
    }

    .widget-buttons-grid > flowbox > flowboxchild > button:active{
      background-color: @base03;
    }

    .widget-buttons-grid > flowbox > flowboxchild > button:checked {
      background-color: @base0D;
    }

    .widget-buttons-grid > flowbox > flowboxchild > button:checked label {
      color: @base00;
    }

    /* Volume */

    .widget-volume {
      background-color: @base01;
      padding: 8px;
      margin: 8px;
      border-radius: 12px;
    }

    .widget-volume > box > button {
      background: transparent;
      border: none;
    }

    .per-app-volume {
      padding: 8px;
      margin: 8px;
      border-radius: 12px;
    }

    .widget-volume trough {
      background-color: @base02;
      border: unset;
      border-radius: 12px;
    }

    .widget-volume trough highlight {
      background-color: @base0D;
      color:unset;
      border-radius: 12px;
      min-height: 12px;
    }

    /* Backlight */

    .widget-backlight {
      background-color: @base01;
      padding: 8px;
      margin: 8px;
      border-radius: 12px;
    }

    .widget-backlight trough {
      background-color: @base02;
      border: unset;
      border-radius: 12px;
    }

    .widget-backlight trough highlight {
      background-color: @base0D;
      color:unset;
      border-radius: 12px;
      min-height: 12px;
    }

    /* Mpris */

    .widget-mpris {
      margin: 8px 8px 0px 8px;
      background-color: @base01;
      border-radius:12px
    }

    .widget-mpris-player {
      margin: 8px 8px 0px 8px;
      padding: 8px 8px 0px 8px;
    }

    .widget-mpris button {
      padding: 5px;
      color: @base05;
      border-radius:12px
    }

    .widget-mpris button:hover {
      background-color: @base02;
    }

    .widget-mpris button:active {
      background-color: @base03;
    }

    .widget-mpris button:disabled {
      color: @base02;
    }

    .widget-mpris-album-art {
      border-radius: 12px;
    }

    .widget-mpris-title {
      font-weight: bold;
      font-size: 1.0rem;
    }

    .widget-mpris-subtitle {
      font-size: 0.8rem;
    }
  '';
}
