{ pkgs, inputs, ...}:

{
  imports = [ inputs.anyrun.homeManagerModules.default ];
  programs.anyrun = {
    enable = true;
    config = {
      plugins = with inputs.anyrun.packages.${pkgs.system}; [
        applications
        symbols
        rink
        shell
        translate
        dictionary
        websearch
        # kidex
        # stdin
        # randr
      ];
      x.fraction = 0.5; 
      y.absolute = 8; 
      width.absolute = 400;
      height.absolute = 0;
      layer = "overlay";
      hideIcons = false;
      hidePluginInfo = true;
      ignoreExclusiveZones = false;
      closeOnClick = true;
      showResultsImmediately = false;
      maxEntries = 10;
    };
    extraConfigFiles = {
      "applications.ron".text = ''
        Config(
          desktop_actions: true,
          max_entries: 10,
          terminal: Some("kitty"),
        )
      '';
      "websearch.ron".text = ''
        Config(
          prefix: "?",
          engines: [DuckDuckGo, Google],
        )
      '';
    };
    extraCss = ''
      * {
        font-family: "Inter Variable";
        font-size: 14px;
      }

      #window {
        background-color: transparent;
      }

      box#main {
        padding: 8px;
        border-radius: 12px;
        border: 1px solid @borders;
        background-color: @theme_base_color;
      }

      entry#entry {
        border-radius: 8px;
        box-shadow: none;
      }

      list#main {
        background-color: transparent;
      }
      
      list#main > row {
        padding: 0;
      }
      
      list#main > row:hover {
        border-radius: 8px;
        box-shadow: none;
      }

      list#plugin {
        background-color: transparent;
      }

      row#match {
        border-radius: 8px;
        padding: 12px;
      }

      label#plugin {
        font-size: 14px;
      }

      label#match-title {
        font-size: 14px;
      }

      label#match-desc {
        font-size: 12px;
        opacity: 0.5;
      }
    '';
  };
}
