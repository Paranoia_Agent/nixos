local g = vim.g

-- Map leader key to space bar
g.mapleader = ' '
g.maplocalleader = ' '

-- Enable nvim api for lua LSP
g.lazydev_enabled = true

-- Solve lsp-zero noice conflict
g.lsp_zero_ui_float_border = 0

require("lz.n").load("plugins")
require("options")
require("keymaps")
