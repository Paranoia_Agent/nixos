;; extends

;; Headers
(
 (atx_heading (atx_h1_marker) @text.title.1.marker)
  (#set! conceal "󰎤")
)
(
 (atx_heading (atx_h2_marker) @text.title.2.marker)
  (#set! conceal "󰎩")
)
(
 (atx_heading (atx_h3_marker) @text.title.3.marker)
  (#set! conceal "󰎪")
)
(
 (atx_heading (atx_h4_marker) @text.title.4.marker)
  (#set! conceal "󰎮")
)
(
 (atx_heading (atx_h5_marker) @text.title.5.marker)
  (#set! conceal "󰎱")
)
(
 (atx_heading (atx_h6_marker) @text.title.6.marker)
  (#set! conceal "󰎵")
)

;; Block Quotes
(
 (block_quote_marker) @punctuation.special
  (#offset! @punctuation.special 0 0 0 -1)
  (#set! conceal "▐")
)
(
 (block_continuation) @punctuation.special
  (#eq? @punctuation.special ">")
  (#set! conceal "▐")
)
(
 (block_continuation) @punctuation.special
  (#eq? @punctuation.special "> ")
  (#offset! @punctuation.special 0 0 0 -1)
  (#set! conceal "▐")
)
(
 (block_continuation) @punctuation.special
  ; for indented code blocks
  (#eq? @punctuation.special ">     ")
  (#offset! @punctuation.special 0 0 0 -5)
  (#set! conceal "▐")
)

; Bullet Points
;(
; [(list_marker_plus) (list_marker_star) (list_marker_minus)]
;  @punctuation.special
;  (#offset! @punctuation.special 0 0 0 -1)
;  (#set! conceal "•")
;)
;(
; [(list_marker_plus) (list_marker_star) (list_marker_minus)]
;  @punctuation.special
;  (#any-of? @punctuation.special "+" "*" "-")
;  (#set! conceal "•")
;)

;; Ckeckboxes
;(
; (task_list_marker_checked) @text.todo.checked
;  (#offset! @text.todo.checked 0 -2 0 0)
;  (#set! conceal "󰄲")
;)
;(
; (task_list_marker_unchecked) @text.todo.unchecked
;  (#offset! @text.todo.unchecked 0 -2 0 0)
;  (#set! conceal "󰄱")
;)
