local keymap = vim.keymap

local diagnostics_active = true
local toggle_diagnostics = function()
  diagnostics_active = not diagnostics_active
  if diagnostics_active then
    vim.diagnostic.show()
  else
    vim.diagnostic.hide()
  end
end

-- Toggle diagnostics
keymap.set('n', '<leader>td', toggle_diagnostics, { desc = 'Toggle diagnostics' })

-- Bind nohlsearch
keymap.set('n', '<esc>', '<CMD>nohlsearch<CR>', { silent = true, desc = 'Clean search highlight' })

-- Delete to black hole register
keymap.set({'n', 'v'}, '<leader>d', [["_d]], { desc = 'Delete permanently'})

-- Rebind vertical movement
keymap.set('n' , '<C-u>' , '<C-u>zz' , { desc = 'Move up half-page and center' })
keymap.set('n' , '<C-d>' , '<C-d>zz' , { desc = 'Move down half-page and center' })
keymap.set('n' , '<C-b>' , '<C-b>zz' , { desc = 'Move UP full-page and center' })
keymap.set('n' , '<C-f>' , '<C-f>zz' , { desc = 'Move DOWN full-page and center' })
keymap.set('n' , '}'     , '}zz'     , { desc = 'Move to next paragraph' })
keymap.set('n' , '{'     , '{zz'     , { desc = 'Move to previous paragraph' })
keymap.set('n' , 'n'     , 'nzz'     , { desc = 'Move to next search input' })
keymap.set('n' , 'N'     , 'Nzz'     , { desc = 'Move to previous search input' })

-- Up/Down keys on soft-wrapped lines
keymap.set('n', '<Down>', 'gj')
keymap.set('n', '<Up>', 'gk')

-- Mini.filess
keymap.set('n', '<leader>fe', '<CMD>lua MiniFiles.open()<CR>', { desc = 'File explorer' })
keymap.set('n', '-', "<CMD>lua require('mini.files').bufdir()<CR>")
