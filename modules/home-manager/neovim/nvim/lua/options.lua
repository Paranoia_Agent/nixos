local opt = vim.o

-- Share system clipboard
opt.clipboard = 'unnamedplus'

-- Enable relative line numbers
opt.number = true
opt.relativenumber = true
opt.cursorline = true

-- Enable signcolumn
opt.signcolumn = 'yes'

-- Highlight matching parentheses
opt.showmatch = true

-- Better search
opt.incsearch = true
opt.hlsearch = true

-- Check Spelling
opt.spell = false
opt.spelllang = 'en'

-- Set space indentation
opt.tabstop = 2
opt.softtabstop = 2
opt.shiftwidth = 2
opt.expandtab = true

-- Textwrap
opt.wrap = true
opt.breakindent = true

-- Set split
opt.splitright = true
opt.splitbelow = true

-- Keep 8 lines above/below cursor
opt.scrolloff = 8

-- Set folding options
opt.foldenable = false
opt.foldcolumn = '1'
opt.foldmethod = 'expr'
opt.foldexpr = 'nvim_treesitter#foldexpr()'
opt.foldlevel = 20

-- Save undo history to file
opt.undofile = true

-- Enable true color support
opt.termguicolors = true

-- Enable mouse options
opt.mouse = 'a'
opt.mousemoveevent = true

-- Notes conceal
opt.conceallevel = 2

-- Set fill characters for end of buffer and folding
opt.fillchars = [[eob: ,fold: ,foldopen:,foldsep: ,foldclose:,stl:─,stlnc:─]]
