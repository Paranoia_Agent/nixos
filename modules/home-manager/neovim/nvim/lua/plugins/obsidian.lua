return  {
  "obsidian.nvim",
  ft = "markdown",
  keys = {
    { '<leader>oo',  '<CMD>ObsidianOpen<CR>',        mode = 'n', desc = 'Obsidian open' },
    { '<leader>oq',  '<CMD>ObsidianQuickSwitch<CR>', mode = 'n', desc = 'Obsidian quick switch' },
    { '<leader>os',  '<CMD>ObsidianSearch<CR>',      mode = 'n', desc = 'Obsidian search' },
    { '<leader>ol',  '<CMD>ObsidianLinks<CR>',       mode = 'n', desc = 'Obsidian links' },
    { '<leader>ob',  '<CMD>ObsidianBacklinks<CR>',   mode = 'n', desc = 'Obsidian backlinks' },
    { '<leader>ow',  '<CMD>ObsidianWorkspace<CR>',   mode = 'n', desc = 'Obsidian workspace' },
    { '<leader>ot',  '<CMD>ObsidianTemplate<CR>',    mode = 'n', desc = 'Obsidian template' },
    { '<leader>onn', '<CMD>ObsidianNew<CR>',         mode = 'n', desc = 'Obsidian new note' },
    { '<leader>onl', '<CMD>ObsidianLinkNew<CR>',     mode = 'n', desc = 'Obsidian new link' },
    { '<leader>od',  '<CMD>ObsidianDailies<CR>',     mode = 'n', desc = 'Obsidian dailies' },
  },
  after = function()
    require('obsidian').setup({
      workspaces = {
        {
          name = "Vault",
          path = "~/Documents/Obsidian/Vault"
        }
      },

      notes_subdir = "Notes",
      new_notes_location = "notes_subdir",

      log_level = vim.log.levels.INFO,

      preferred_link_style = "wiki",
      disable_frontmatter = true,
      use_advanced_uri = false,
      open_app_foreground = false,

      daily_notes = {
        folder = "Notes/1 - Fleeting Notes",
        alias_format = "%B %-d, %Y",
        date_format = "%Y-%m-%d",
        template = "template-fleeting.md"
      },

      templates = {
        folder = "Assets/Templates",
        date_format = "%Y-%m-%d",
        time_format = "%H:%M",
      },

      picker = {
        name = "telescope.nvim",
        mappings = {},
        note_mappings = {},
        tag_mappings = {},
      },

      sort_by = "modified",
      sort_reversed = true,
      search_max_lines = 1000,
      open_notes_in = "current",

      completion = {
        nvim_cmp = true,
        min_chars = 2
      },

      mappings = {
        -- Overrides the 'gf' mapping to work on markdown/wiki links within your vault.
        ["gf"] = {
          action = function()
            return require("obsidian").util.gf_passthrough()
          end,
          opts = { noremap = false, expr = true, buffer = true },
        },
        -- Toggle check-boxes.
        ["<leader>ch"] = {
          action = function()
            return require("obsidian").util.toggle_checkbox()
          end,
          opts = { buffer = true },
        },
        -- Smart action depending on context, either follow link or toggle checkbox.
        ["<CR>"] = {
          action = function()
            return require("obsidian").util.smart_action()
          end,
          opts = { buffer = true, expr = true },
        }
      },

      --  Set Title with UID
      ---@param title string|?
      ---@return string
      note_id_func = function(title)
        local suffix = ""
        if title ~= nil then
          suffix = title:gsub(" ", "-"):gsub("[^A-Za-z0-9-áéíóúãõàâêôç]", ""):lower()
        else
          suffix = "untitled"
        end
        return tostring(os.date('%Y%m%d%H%M')) .. "-" .. suffix
      end,

      ui = {
        enable = true,
        update_debounce = 200,
        max_file_length = 5000,
        checkboxes = {
          [" "] =  { char = "󰄱", hl_group = "ObsidianTodo",       order = 1 },
          ["/"] =  { char = "󰡖", hl_group = "ObsidianIncomplete", order = 2 },
          ["x"] =  { char = "󰄲", hl_group = "ObsidianDone",       order = 3 },
          ["-"] =  { char = "", hl_group = "ObsidianCanceled",   order = 5 },
          [">"] =  { char = "", hl_group = "ObsidianForwarded",  order = 6 },
          ["<"] =  { char = "", hl_group = "ObsidianScheduling", order = 7 },
          ["!"] =  { char = "", hl_group = "ObsidianImportant",  order = 8 },
        },

        bullets            = { char = "•", hl_group = "ObsidianBullet" },
        external_link_icon = { char = "", hl_group = "ObsidianExtLinkIcon" },

        reference_text = { hl_group = "ObsidianRefText" },
        highlight_text = { hl_group = "ObsidianHighlightText" },
        tags           = { hl_group = "ObsidianTag" },
        block_ids      = { hl_group = "ObsidianBlockID" },

        hl_groups = {
          ObsidianTodo       = { bold      = true, fg = "#f78c6c" },
          ObsidianIncomplete = { bold      = true, fg = "#f78c6c" },
          ObsidianDone       = { bold      = true, fg = "#89ddff" },
          ObsidianCanceled   = { bold      = true, fg = "#ff5370" },
          ObsidianForwarded  = { bold      = true, fg = "#f78c6c" },
          ObsidianScheduling = { bold      = true, fg = "#f78c6c" },
          ObsidianImportant  = { bold      = true, fg = "#d73128" },
          ObsidianBullet     = { bold      = true, fg = "#89ddff" },
          ObsidianRefText    = { underline = true, fg = "#c792ea" },
          ObsidianTag        = { italic    = true, fg = "#89ddff" },
          ObsidianBlockID    = { italic    = true, fg = "#89ddff" },
          ObsidianExtLinkIcon   = { fg = "#c792ea" },
          ObsidianHighlightText = { bg = "#75662e" },
        },
      },

      ---@param url string
      follow_url_func = function(url)
        -- Open the URL in the default web browser.
        vim.ui.open(url) -- need Neovim 0.10.0+
      end,

      ---@param img string
      follow_img_func = function(img)
        vim.fn.jobstart({"xdg-open", img})  -- linux
        -- vim.fn.jobstart { "qlmanage", "-p", img }  -- Mac OS quick look preview
        -- vim.cmd(':silent exec "!start ' .. url .. '"') -- Windows
      end,

      attachments = {
        img_folder = "Assets/Images",
        confirm_img_paste = true,
        ---@param client obsidian.Client
        ---@param path obsidian.Path
        ---@return string
        img_text_func = function(client, path)
          path = client:vault_relative_path(path) or path
          return string.format("![%s](%s)", path.name, path)
        end,
      },

    })
  end
}
