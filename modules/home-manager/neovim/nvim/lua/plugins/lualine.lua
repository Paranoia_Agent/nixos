return {
  "lualine.nvim",
  event = "DeferredUIEnter",
  after = function()
    -- Macro display module
    local function extra_mode_status()
      -- recording macros
      local reg_recording = vim.fn.reg_recording()
      if reg_recording ~= '' then
        return '@' .. reg_recording
      end
      return ''
    end

    -- Remove lualine colors
    local default_theme = {
      normal = {
        a = { fg = 'none', bg = 'none' },
        b = { fg = 'none', bg = 'none' },
        c = { fg = 'none', bg = 'none' },
        x = { fg = 'none', bg = 'none' },
        y = { fg = 'none', bg = 'none' },
        z = { fg = 'none', bg = 'none' },
      },
    }

    -- Lualine options
    require('lualine').setup {
      globalstatus = true,
      options = {
        theme = default_theme,
        component_separators = '',
        section_separators = { left = ''; right = ''},
      },

      sections= {
        lualine_a = { {'mode', fmt = string.lower } },
        lualine_b = { 'branch' },
        lualine_c = {
          {
            'diff',
            colored = true,
            diff_color = {
              added = 'DiffAdd',
              modified = 'DiffChange',
              removed = 'DiffDelete',
            },
          },
          'diagnostics',
        },
        lualine_x = { 'filename' },
        lualine_y = { 'filetype' },
        lualine_z = { 'location', {extra_mode_status} }
      },

      tabline = {},
      winbar = {},
      extensions = { 'fzf', 'toggleterm', 'quickfix' }
    }
  end
}
