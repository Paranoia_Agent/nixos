return {
  {
    "nvim-cmp",
    event = "InsertEnter",
    after = function()
      local lsp_zero = require('lsp-zero')
      lsp_zero.extend_cmp()

      local cmp = require('cmp')
      local cmp_action = require('lsp-zero').cmp_action()
      local cmp_format = require('lsp-zero').cmp_format({details = true})

      -- Load friendly-snippets into nvim-cmp
      require("luasnip.loaders.from_vscode").lazy_load()

      cmp.setup({
        sources = {
          -- Snippet sources
          {name = 'nvim_lsp'},
          {name = 'luasnip'},
          {name = 'buffer'},
          {name = 'path'},
        },

        -- `/` cmdline setup.
        cmp.setup.cmdline('/', {
          mapping = cmp.mapping.preset.cmdline(),
          sources = {
            { name = 'buffer' }
          }
        }),

        -- `:` cmdline setup.
        cmp.setup.cmdline(':', {
          mapping = cmp.mapping.preset.cmdline(),
          sources = cmp.config.sources({
            { name = 'path' }
          }, {
              {
                name = 'cmdline',
                option = {
                  ignore_cmds = { 'Man', '!' }
                }
              }
            })
        }),

        window = {
          -- Add window borders
          completion = cmp.config.window.bordered(),
          documentation = cmp.config.window.bordered(),
        },

        mapping = {
          -- Custom Mappings
          ['<CR>']    = cmp.mapping.confirm({select = false}),
          ['<Tab>']   = cmp_action.luasnip_supertab(),
          ['<S-Tab>'] = cmp_action.luasnip_shift_supertab(),
          -- Default Mappings
          ['<C-y>']   = cmp.mapping.confirm({select = false}),
          ['<C-e>']   = cmp.mapping.abort(),
          ['<Up>']    = cmp.mapping.select_prev_item({behavior = 'select'}),
          ['<Down>']  = cmp.mapping.select_next_item({behavior = 'select'}),
          ['<C-p>']   = cmp.mapping(function()
            if cmp.visible() then
              cmp.select_prev_item({behavior = 'insert'})
            else
              cmp.complete()
            end
          end),
          ['<C-n>'] = cmp.mapping(function()
            if cmp.visible() then
              cmp.select_next_item({behavior = 'insert'})
            else
              cmp.complete()
            end
          end),
        },

        snippet = {
          expand = function(args)
            require('luasnip').lsp_expand(args.body)
          end,
        },

        formatting = cmp_format

      })
    end
  },
  { "luasnip"           , event = "InsertEnter", },
  { "friendly-snippets" , event = "InsertEnter", },
  { "cmp-nvim-lsp"      , event = "InsertEnter", },
  { "cmp_luasnip"       , event = "InsertEnter", },
  { "cmp-buffer"        , event = "InsertEnter", },
  { "cmp-cmdline"       , event = "InsertEnter", },
  { "cmp-path"          , event = "InsertEnter", },
}
