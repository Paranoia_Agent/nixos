return {
  "mini.nvim",
  event = "DeferredUIEnter",
  after = function ()
    local ai          = require('mini.ai')
    local align       = require('mini.align')
    local basics      = require('mini.basics')
    local bracketed   = require('mini.bracketed')
    local clue        = require('mini.clue')
    local diff        = require('mini.diff')
    local files       = require('mini.files')
    local git         = require('mini.git')
    local indentscope = require('mini.indentscope')
    local operators   = require('mini.operators')
    local pairs       = require('mini.pairs')
    local splitjoin   = require('mini.splitjoin')
    local surround    = require('mini.surround')

    local go_in_plus = function()
      for _ = 1, vim.v.count1 do
        MiniFiles.go_in({ close_on_file = true })
      end
    end

    local go_out_plus = function()
      for _ = 1, vim.v.count1 do
        MiniFiles.go_out()
        MiniFiles.trim_right()
      end
    end

    ai.setup()
    align.setup()
    bracketed.setup()
    git.setup()
    operators.setup()
    pairs.setup()
    splitjoin.setup()
    surround.setup()

    basics.setup({
      mappings = {
        windows = true,
      }
    })

    indentscope.setup({
      options = {
        try_as_border = true
      },
      symbol = "│",
    })

    diff.setup({
      view = {
        style = 'sign',
        signs = { add = '┃', change = '┃', delete = '┃' },
        priority = 199,
      },
    })

    clue.setup({
      triggers = {
        -- Leader triggers
        { mode = 'n', keys = '<Leader>' },
        { mode = 'x', keys = '<Leader>' },
        -- Built-in completion
        { mode = 'i', keys = '<C-x>' },
        -- `g` key
        { mode = 'n', keys = 'g' },
        { mode = 'x', keys = 'g' },
        -- `s` key
        { mode = 'n', keys = 's' },
        { mode = 'x', keys = 's' },
        -- Marks
        { mode = 'n', keys = "'" },
        { mode = 'n', keys = '`' },
        { mode = 'x', keys = "'" },
        { mode = 'x', keys = '`' },
        -- Registers
        { mode = 'n', keys = '"' },
        { mode = 'x', keys = '"' },
        { mode = 'i', keys = '<C-r>' },
        { mode = 'c', keys = '<C-r>' },
        -- Window commands
        { mode = 'n', keys = '<C-w>' },
        -- `z` key
        { mode = 'n', keys = 'z' },
        { mode = 'x', keys = 'z' },
      },

      clues = {
        -- Enhance this by adding descriptions for <Leader> mapping groups
        clue.gen_clues.builtin_completion(),
        clue.gen_clues.g(),
        clue.gen_clues.marks(),
        clue.gen_clues.registers(),
        clue.gen_clues.windows(),
        clue.gen_clues.z(),
      },

      window = {
        -- Delay before showing clue window
        delay = 0,
        -- Keys to scroll inside the clue window
        scroll_down = '<C-d>',
        scroll_up = '<C-u>',
      }
    })

    files.setup({
      -- No need to copy this inside `setup()`. Will be used automatically.
      content = {
        filter = nil,
        prefix = nil,
        sort = nil,
      },

      mappings = {
        close       = 'q',
        go_in       = 'l',
        go_in_plus  = 'L',
        go_out      = 'h',
        go_out_plus = 'H',
        reset       = '<BS>',
        reveal_cwd  = '@',
        show_help   = 'g?',
        synchronize = '=',
        trim_left   = '<',
        trim_right  = '>',
      },

      options = {
        permanent_delete = false,
        use_as_default_explorer = true,
      },

      windows = {
        max_number = math.huge,
        preview = false,
        width_focus = 50,
        width_nofocus = 15,
        width_preview = 25,
      },
    })

    -- Check if current buffer exists
    files.bufdir = function ()
      local path = vim.bo.buftype ~= "nofile" and vim.api.nvim_buf_get_name(0) or nil
      MiniFiles.open(path)
      -- Display cwd
      MiniFiles.reveal_cwd()
    end

    -- Mini.files extra mappings
    vim.api.nvim_create_autocmd('User', {
      pattern = 'MiniFilesBufferCreate',
      callback = function(args)
        local map_buf = function(lhs, rhs) vim.keymap.set('n', lhs, rhs, { buffer = args.data.buf_id }) end

        map_buf('<Right>'  , MiniFiles.go_in)
        map_buf('<S-Right>', go_in_plus)
        map_buf('<Left>'   , MiniFiles.go_out)
        map_buf('<S-Left>' , go_out_plus)
        map_buf('<ESC>'    , MiniFiles.close)
        map_buf('<CR>'     , go_in_plus)

      end,
    })

  end
}
