return {
  "toggleterm.nvim",
  keys = { [[<c-\>]] },
  after = function()
    require('toggleterm').setup{
      size = 20,
      direction = 'float',
      open_mapping = [[<c-\>]],
      hide_numbers = true,
      autochdir = true,
      start_in_insert = true,
      close_on_exit = true,
      float_opts = {
        border = 'curved',
      }
    }
  end
}
