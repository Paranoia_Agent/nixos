return {
  {
    "base16-nvim",
    enabled = true,
    priority = 1000,
    after = function()
      require('base16-colorscheme').with_config({
        telescope = false,
        indentblankline = true,
        notify = true,
        ts_rainbow = true,
        cmp = true,
        illuminate = true,
        dapui = true
      })

      -- vim.cmd('colorscheme base16-horizon-dark')
      vim.cmd('colorscheme base16-chalk')

      -- Set cursorline colors
      vim.api.nvim_set_hl(0, 'CursorLineNR',       { fg = require('base16-colorscheme').colors.base04 })
      vim.api.nvim_set_hl(0, 'LineNR',             { fg = require('base16-colorscheme').colors.base02 })
      vim.api.nvim_set_hl(0, 'TabLineSel',         { fg = require('base16-colorscheme').colors.base0D})

      -- Mini.diff colors
      -- vim.api.nvim_set_hl(0, 'MiniDiffSignAdd',    { fg = require('base16-colorscheme').colors.base0B})
      -- vim.api.nvim_set_hl(0, 'MiniDiffSignChange', { fg = require('base16-colorscheme').colors.base0D})
      -- vim.api.nvim_set_hl(0, 'MiniDiffSignDelete', { fg = require('base16-colorscheme').colors.base08})

      -- Horizon dark diff
      vim.api.nvim_set_hl(0, 'MiniDiffSignAdd',    { fg = '#29d398'})
      vim.api.nvim_set_hl(0, 'DiffAdd',            { fg = '#29d398'})
      vim.api.nvim_set_hl(0, 'DiffAdded',          { fg = '#29d398'})

      vim.api.nvim_set_hl(0, 'MiniDiffSignChange', { fg = '#26bbd9'})
      vim.api.nvim_set_hl(0, 'DiffChange',         { fg = '#26bbd9'})
      vim.api.nvim_set_hl(0, 'DiffChanged',        { fg = '#26bbd9'})

      vim.api.nvim_set_hl(0, 'MiniDiffSignDelete', { fg = '#e95678'})
      vim.api.nvim_set_hl(0, 'DiffDelete',         { fg = '#e95678'})
      vim.api.nvim_set_hl(0, 'DiffDeleted',        { fg = '#e95678'})
    end
  }
}
