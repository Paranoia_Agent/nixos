return {
  {
    "nvim-treesitter",
    event = "DeferredUIEnter",
    after = function()
      require('nvim-treesitter.configs').setup {
        auto_install = false,
        sync_install = true,

        highlight = {
          enable = true,
          additional_vim_regex_highlighting = false,
        },
        indent = { enable = true, },

        ensure_installed = {},
        ignore_install = {},
        modules = {}
      }
    end
  },
  {
    "nvim-treesitter-context",
    event = "DeferredUIEnter",
    after = function ()
      require'treesitter-context'.setup{
        enable = true,
        max_lines = 0,
        min_window_height = 0,
        line_numbers = true,
        multiline_threshold = 20,
        trim_scope = 'outer',
        mode = 'cursor',
        separator = nil,
        zindex = 20,
        on_attach = nil
      }
    end
  }
}
