return {
  {
    "lsp-zero.nvim",
    after = function ()
      vim.g.lsp_zero_extend_cmp = 0
      vim.g.lsp_zero_extend_lspconfig = 0
    end
  },
  {
    "nvim-lspconfig",
    cmd = 'LspInfo',
    event = {'BufReadPre', 'BufNewFile'},
    after = function()
      local lsp_zero = require('lsp-zero')
      lsp_zero.extend_lspconfig()

      lsp_zero.on_attach(function(client, bufnr)
        -- see :help lsp-zero-keybindings
        -- to learn the available actions
        lsp_zero.default_keymaps({buffer = bufnr})
      end)

      lsp_zero.set_sign_icons({
        error = '󰅚',
        warn = '⚠',
        hint = '󰌶',
        info = 'ⓘ'
      })

      require'lazydev'.setup()
      require'lspconfig'.lua_ls.setup{}
      require'lspconfig'.nixd.setup{}
    end
  },
  { "lazydev.nvim", }
}
