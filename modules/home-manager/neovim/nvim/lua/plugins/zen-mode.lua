return {
  "zen-mode.nvim",
  cmd = "ZenMode",
  after = function()
    require("zen-mode").setup({
      window = {
        backdrop = 1,
        width = 120,
        height = 1,
        options = {
          -- signcolumn = "no", -- disable signcolumn
          -- number = false, -- disable number column
          -- relativenumber = false, -- disable relative numbers
          -- cursorline = false, -- disable cursorline
          -- cursorcolumn = false, -- disable cursor column
          -- foldcolumn = "0", -- disable fold column
          -- list = false, -- disable whitespace characters
        },
      },
      plugins = {
        options = {
          enabled = true,
          ruler = false,
          showcmd = false,
          laststatus = 0,
        },
        twilight = { enabled = true },
        gitsigns = { enabled = true },
        tmux = { enabled = false },
        todo = { enabled = false },
      },
    })
  end
}
