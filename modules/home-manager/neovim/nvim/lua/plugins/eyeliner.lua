return {
  "eyeliner.nvim",
  event = "BufReadPost",
  after = function()
    require('eyeliner').setup {
      highlight_on_key = true,
      dim = true
    }
  end
}
