return {
  "statuscol.nvim",
  event = "DeferredUIEnter",
  after = function()
    local builtin = require('statuscol.builtin')

    require('statuscol').setup {
      setopt = true,
      relculright = true,

      segments = {
        -- { text = { builtin.foldfunc }, click = "v:lua.ScFa" },
        {
          sign = { namespace = { "diagnostic" }, maxwidth = 1, colwidth = 2, auto = true, foldclosed = true },
          click = "v:lua.ScSa"
        },
        {text = { builtin.lnumfunc }, click = "v:lua.ScLa"},
        {
          sign = { name = { ".*" }, text = { ".*" }, maxwidth = 1, colwidth = 2, auto = false, foldclosed = true },
          click = "v:lua.ScSa",
        },
      }
    }
  end
}
