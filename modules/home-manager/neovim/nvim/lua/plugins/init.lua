return {
  -- CMD
  {
    "vim-table-mode",
    cmd = "TableModeToggle",
    keys = { '<leader>tm', },
    after = function ()
      vim.g.table_mode_corner = "|"
    end
  },
  -- Loaded Early
  { "nui.nvim" },
  { "nvim-notify" },
  { "nvim-web-devicons" },
  { "plenary.nvim" },
  { "twilight.nvim", },
}
