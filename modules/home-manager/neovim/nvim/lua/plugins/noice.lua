return {
  "noice.nvim",
  event = "DeferredUIEnter",
  keys = {
    { '<leader>nh', '<CMD>Noice history<CR>', mode = 'n', desc = 'Notification history' },
    { '<leader>nl', '<CMD>Noice last<CR>', mode = 'n', desc = 'Notification last' },
    { '<leader>nd', '<CMD>Noice dismiss<CR>', mode = 'n', desc = 'Notification dismiss' },
    { '<leader>ne', '<CMD>Noice errors<CR>', mode = 'n', desc = 'Notification errors' }
  },
  after = function()
    require('noice').setup({
      lsp = {
        -- Override markdown rendering
        override = {
          ["vim.lsp.util.convert_input_to_markdown_lines"] = true,
          ["vim.lsp.util.stylize_markdown"] = true,
          ["cmp.entry.get_documentation"] = true, -- requires hrsh7th/nvim-cmp
        },
      },
      -- Noice presets
      presets = {
        bottom_search = false, -- use a classic bottom cmdline for search
        command_palette = true, -- position the cmdline and popupmenu together
        long_message_to_split = true, -- long messages will be sent to a split
        inc_rename = false, -- enables an input dialog for inc-rename.nvim
        lsp_doc_border = false -- add a border to hover docs and signature help
      }
    })
  end
}
