return {
  { "telescope-fzf-native.nvim" },
  {
    "telescope.nvim",
    cmd = "Telescope",
    keys = {
      -- Telescope keymaps
      { '<leader>ff',  '<CMD>Telescope find_files<CR>',  mode = 'n', desc = 'Find files' },
      { '<leader>fb',  '<CMD>Telescope buffers<CR>',     mode = 'n', desc = 'Find buffers' },
      { '<leader>fp',  '<CMD>Telescope git_files<CR>',   mode = 'n', desc = 'Find project files'  },
      { '<leader>fr',  '<CMD>Telescope oldfiles<CR>',    mode = 'n', desc = 'Find recent files'  },
      { '<leader>fR',  '<CMD>Telescope registers<CR>',   mode = 'n', desc = 'Find registers' },
      { '<leader>fg',  '<CMD>Telescope live_grep<CR>',   mode = 'n', desc = 'Find live grep' },
      { '<leader>fG',  '<CMD>Telescope grep_string<CR>', mode = 'n', desc = 'Find grep string' },
      { '<leader>fj',  '<CMD>Telescope jumplist<CR>',    mode = 'n', desc = 'Find jumplist' },
      { '<leader>fd',  '<CMD>Telescope diagnostics<CR>', mode = 'n', desc = 'Find diagnostics' },
      { '<leader>fc',  '<CMD>Telescope git_commits<CR>', mode = 'n', desc = 'Find commits' },
      { '<leader>fs',  '<CMD>Telescope git_status<CR>',  mode = 'n', desc = 'Find status' },
      { '<leader>fh',  '<CMD>Telescope help_tags<CR>',   mode = 'n', desc = 'Find help' },
      { '<leader>fn',  '<CMD>Telescope noice<CR>',       mode = 'n', desc = 'Find notifications' },
      -- Obsidian Keymaps
      { '<leader>oo',  '<CMD>ObsidianOpen<CR>',          mode = 'n', desc = 'Obsidian open' },
      { '<leader>oq',  '<CMD>ObsidianQuickSwitch<CR>',   mode = 'n', desc = 'Obsidian quick switch' },
      { '<leader>os',  '<CMD>ObsidianSearch<CR>',        mode = 'n', desc = 'Obsidian search' },
      { '<leader>ol',  '<CMD>ObsidianLinks<CR>',         mode = 'n', desc = 'Obsidian links' },
      { '<leader>ob',  '<CMD>ObsidianBacklinks<CR>',     mode = 'n', desc = 'Obsidian backlinks' },
      { '<leader>ow',  '<CMD>ObsidianWorkspace<CR>',     mode = 'n', desc = 'Obsidian workspace' },
      { '<leader>ot',  '<CMD>ObsidianTemplate<CR>',      mode = 'n', desc = 'Obsidian template' },
      { '<leader>onn', '<CMD>ObsidianNew<CR>',           mode = 'n', desc = 'Obsidian new note' },
      { '<leader>onl', '<CMD>ObsidianLinkNew<CR>',       mode = 'n', desc = 'Obsidian new link' },
      { '<leader>od',  '<CMD>ObsidianDailies<CR>',       mode = 'n', desc = 'Obsidian dailies' },
    },
    after = function()
      local telescope = require('telescope')
      local actions = require('telescope.actions')
      local actions_layout = require('telescope.actions.layout')

      telescope.setup {
        defaults = {
          layout_strategy = 'flex',
          sorting_strategy = "ascending",
          layout_config = {
            prompt_position = "top",
            height = 0.80,
            width = 0.75,
            horizontal = {
              preview_width = 0.50,
            },
            vertical = {
              preview_height = 0.45,
              mirror = true,
            },
          },
          mappings = {
            i = {
              ['<ESC>'] = actions.close,
              ['<C-b>'] = actions.delete_buffer + actions.move_to_top,
              ['<C-p>'] = actions_layout.toggle_preview,
              ['<C-h>'] = "which_key"
            },
          },
          set_env = { ['COLORTERM'] = 'truecolor' },
          prompt_prefix = '  ',
          selection_caret = '  ',
          entry_prefix = '  ',
          initial_mode = 'insert',
        },
        extensions = {
          fzf = {
            fuzzy = true,
            override_generic_sorter = true,
            override_file_sorter = true,
            case_mode = "smart_case"
          }
        }
      }
      telescope.load_extension('fzf')
      telescope.load_extension('noice')
    end
  }
}
