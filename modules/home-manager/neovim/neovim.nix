{ pkgs, inputs, ... }:

let
  lz-n = pkgs.vimUtils.buildVimPlugin {
    name = "lz.n";
    src = inputs.lz-n;
  };
in {
  # Manage neovim plugins with home manager
  programs.neovim = {
    enable = true;

    plugins = with pkgs.vimPlugins; [
      lz-n
      
      # Treesitter
      {
        plugin = nvim-treesitter.withPlugins
        (p:[
          p.lua 
          p.nix
          p.regex
          p.markdown
          p.markdown_inline
          p.vim
          p.vimdoc
        ]);
      }
      { plugin = nvim-treesitter-context;   optional = true; }

      # Snippets
      { plugin = luasnip;                   optional = false; }
      { plugin = friendly-snippets;         optional = false; }

      # CMP
      { plugin = nvim-cmp;                  optional = false; }
      { plugin = cmp-nvim-lsp;              optional = false; }
      { plugin = cmp_luasnip;               optional = false; }
      { plugin = cmp-buffer;                optional = false; }
      { plugin = cmp-cmdline;               optional = false; }
      { plugin = cmp-path;                  optional = false; }

      # LSP
      { plugin = lsp-zero-nvim;             optional = true; }
      { plugin = nvim-lspconfig;            optional = true; }
      { plugin = lazydev-nvim;              optional = true; }

      # Navigation/Editing
      { plugin = telescope-nvim;            optional = true; }
      { plugin = telescope-fzf-native-nvim; optional = true; }
      { plugin = mini-nvim;                 optional = true; }
      { plugin = toggleterm-nvim;           optional = true; }
      { plugin = eyeliner-nvim;             optional = true; }

      # UI
      { plugin = base16-nvim;               optional = true; }
      { plugin = noice-nvim;                optional = true; }
      { plugin = lualine-nvim;              optional = true; }
      { plugin = statuscol-nvim;            optional = true; }

      # Note-taking/Markdown
      { plugin = obsidian-nvim;             optional = true; }
      { plugin = zen-mode-nvim;             optional = true; }
      { plugin = twilight-nvim;             optional = true; }
      { plugin = vim-table-mode;            optional = true; }

      # Dependencies
      { plugin = nui-nvim;                  optional = true; }
      { plugin = nvim-notify;               optional = true; }
      { plugin = nvim-web-devicons;         optional = true; }
      { plugin = plenary-nvim;              optional = true; }
    ];

    extraPackages = with pkgs; [
      # General Packages
      xclip
      wl-clipboard
      ripgrep

      # Language Servers
      lua-language-server
      nixd
    ];

    # Luarocks dependencies
    # extraLuaPackages = luaPkgs: [
    # ];
  };

  # Symlink neovim config
  home.file = {
    ".config/nvim" = {
      source = ./nvim/.;
      recursive = true;
    };
  };
}
