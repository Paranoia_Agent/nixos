{
  programs.kitty = {
    enable = true;
    font = {
      # name = "CaskaydiaCove NFM";
      # name = "JetBrainsMonoNL NFM";
      name = "Maple Mono NF";
      size = 12;
    };
    extraConfig = ''
      window_padding_width 5
      mouse_hide_wait 3
      update_check_interval 0
      confirm_os_window_close 0
      enable_audio_bell false
      hide_window_decorations false
      modify_font underline_position 6px
      modify_font underline_thickness 200%
    '';
  };
}
