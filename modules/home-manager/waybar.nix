{
  programs.waybar = {
    enable = true;
    systemd.enable = true;
    systemd.target = "niri-session.target";
  };

  programs.waybar.settings = {
    mainBar = {
      layer = "top";
      modules-left = [ "custom/launcher" "niri/workspaces" "niri/window" ];
      modules-center = [ "clock" ];
      modules-right = [ "network" "pulseaudio" "backlight" "battery" "idle_inhibitor" "bluetooth" "custom/notification" ];

      "custom/launcher" = {
        format = "󱄅";
        tooltip = false;
        on-click = "anyrun";
      };

      "niri/workspaces" = {
        format = "{icon}";
        format-icons = {
          active = "";
          default = "";
        };
      };

      "niri/window" = {
        format= "   {title}";
        max-length = 40;
        separate-outputs = true;
        rewrite = {
          "^.*( — Zen Browser|Zen Browser)$" = "󰺕   Zen Browser";
          "^.*( — Firefox|Firefox)$" = "   Firefox";
          "^.*( — Chromium|Chromium)$" = "   Chromium";
          "^.*( — Text Editor|Text Editor)$" = "󱞁   Text Editor";
          "^.*(Steam)$" = "󰓓   Steam";
          "^.*(Discord.*)$" = "   Discord";
          "^.*(nvim)$" = "   Neovim";
          "^.*(~.*|/.*)$" = "󰄛   Kitty";
        };
      };

      "clock" = {
        format = "{:%b %d - %H:%M}" ;
        tooltip-format = "<tt><small>{calendar}</small></tt>";
        calendar = {
          mode = "month";
          mode-mon-col = 3;
          on-scroll = 1;
        };
        actions =  {
          on-click = "mode";
          on-scroll-up = "shift_up";
          on-scroll-down = "shift_down";
        };
      };
      
      "network" = {
        format = "{ifname}";
        format-wifi = "   {essid}";
        format-ethernet = "   Connected";
        format-disconnected = "";
        tooltip = false;
        max-length = 50;
      };

      "pulseaudio" = {
        format = "{icon}   {volume}%";
        format-bluetooth = "{icon}    {volume}%";
        format-muted = "   Muted";
        format-icons = {
          headphone = "";
          hands-free = "";
          headset = "";
          phone = "";
          portable = "";
          car = "";
          default = ["" "" ""];
        };
        tooltip = false;
        scroll-step = 5;
        on-click = "pactl set-sink-mute @DEFAULT_SINK@ toggle";
        ignored-sinks = ["Easy Effects Sink"];
      };

      "backlight" = {
        format = "{icon}   {percent}%";
        format-icons = ["󰃞" "󰃟" "󰃠"];
        tooltip = false;
        on-scroll-up = "brightnessctl set 1%+";
        on-scroll-down = "brightnessctl set 1%-";
        scroll-step = 5;
        device = "acpi_video0";
      };

      "battery" = {
        interval = 60;
        format = "{icon} {capacity}%";
        format-icons = ["󰁻" "󰁽" "󰁿" "󰂁" "󰁹"];
        tooltip = false;
        max-length = 25;
      };

      "idle_inhibitor" = {
        format = "{icon}";
        format-icons = {
          activated = "󰅶";
          deactivated = "󰾪";
        };
        tooltip = false;
      };

      "bluetooth" = {
        format-on = "󰂲";
        format-off = "󰂲";
        format-connected = "󰂯";
        tooltip = false;
      };

      "custom/notification" = {
        tooltip = false;
        format = "{} {icon}";
        format-icons = {
          notification = "󱅫";
          none = "󰂚";
          dnd-notification = "󰂛";
          dnd-none = "󰂛";
          inhibited-notification = "󰂜";
          inhibited-none = "󰂜";
          dnd-inhibited-notification = "󰪑";
          dnd-inhibited-none = "󰪑";
        };
        return-type = "json";
        exec-if = "which swaync-client";
        exec = "swaync-client -swb";
        on-click = "swaync-client -t -sw";
        on-click-right = "swaync-client -d -sw";
        escape = true;
      };
    };
  };
  programs.waybar.style = ''
    * {
      border: none;
      border-radius: 0;
      font-family: "Inter Variable", "Symbols Nerd Font Mono";
      font-weight: bold;
      font-size: 14px;
    }

    window#waybar {
      /*background: transparent;*/
      background: @base00;
      color: @base05;
    }

    .modules-left {
      margin-left: 4px;
    }

    .modules-right {
      margin-right: 4px;
    }

    tooltip {
      background: transparent;
      border: none;
    }

    tooltip label {
      background: @base01;
      border-radius: 12px;
      border: 1px solid @base02;
      padding: 15px;
    }

    tooltip * {
      text-shadow: none;
    }

    #custom-launcher,
    #workspaces,
    #window,
    #clock,
    #custom-notification,
    #battery,
    #backlight,
    #pulseaudio,
    #network,
    #bluetooth,
    #idle_inhibitor {
      background: @base02;
      opacity: 1;
      padding: 0px 10px;
      margin: 8px 4px;
      border-radius: 12px;
    }
    
    #custom-launcher {
      margin-left: 8px;
      color: @base00;
      background-color: @base0D;
    }

    #workspaces {
      padding: 0px 0px;
    }

    #workspaces button {
      color: @base05;
      background: @base02;
      border-radius: 12px;
    }

    #workspaces button.active {
      color: @base00;
      background: @base0D;
      border-radius: 12px;
    }

    window#waybar.empty #window {
      background: none;
      background-color: transparent;
      border: none;
    }

    #custom-notification {
      margin-right: 8px;
      color: @base00;
      background-color: @base0A;
    }
  '';
}
