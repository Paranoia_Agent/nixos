{ inputs, ... }:

{
  home = {
    username = "paranoia-agent";
    homeDirectory = "/home/paranoia-agent";
    stateVersion = "24.05";
    sessionVariables = {
      EDITOR = "nvim";
      SUDO_EDITOR = "nvim";
    };
  };

  imports = [
    # System config.
    ../../modules/home-manager/dconf.nix

    # Terminal related
    ../../modules/home-manager/zsh.nix
    ../../modules/home-manager/kitty.nix

    # Desktop / Window Manager
    ../../modules/home-manager/niri/niri.nix
    ../../modules/home-manager/niri/niri-quote.nix

    # General packages and applications
    ../../modules/home-manager/applications.nix

    # Neovim
    ../../modules/home-manager/neovim/neovim.nix
  ];

  programs.home-manager.enable = true;
}
