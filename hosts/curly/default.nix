{ inputs, ... }:

{
  imports = [
      # NixOS and Home Manager
      ../../modules/nixos/configuration.nix
      ./hardware-configuration.nix
      inputs.home-manager.nixosModules.default

      # Desktop Environment / Window Manager
      ../../modules/nixos/gnome.nix
      ../../modules/nixos/niri.nix

      # System fonts
      ../../modules/nixos/fonts.nix

      # System theming
      ../../modules/nixos/theming.nix
      { scheme = "${inputs.tt-schemes}/base16/horizon-terminal-dark.yaml"; }
    ];
    
  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "curly"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Configure keymap in X11
  services.xserver = {
    xkb.layout = "us, br";
    xkb.variant = "colemak_dh_iso,";
    xkb.options = "caps:swapescape, altwin:swap_lalt_lwin, grp:menu_toggle";
  };

  # Home-manager module
  home-manager = {
    useGlobalPkgs = true;
    extraSpecialArgs = {inherit inputs;};
    users = {
      "paranoia-agent" = import ../../homes/curly/home.nix;
    };
  };

  system.stateVersion = "23.11";
}
