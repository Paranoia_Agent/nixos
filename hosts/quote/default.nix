{ config, inputs, ... }:

{
  imports = [ 
      # NixOS and Home Manager
      ../../modules/nixos/configuration.nix
      ./hardware-configuration.nix
      inputs.home-manager.nixosModules.default

      # Desktop Environment / Window Manager
      ../../modules/nixos/gnome.nix
      ../../modules/nixos/niri.nix

      # System fonts
      ../../modules/nixos/fonts.nix

      # System theming
      ../../modules/nixos/theming.nix
      # { scheme = "${inputs.tt-schemes}/base16/horizon-terminal-dark.yaml"; }
      { scheme = "${inputs.tt-schemes}/base16/chalk.yaml"; }
    ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Enable NVIDIA Kernel Modules
  hardware.graphics = {
    enable = true;
    enable32Bit = true;
  };

  services.xserver.videoDrivers = ["nvidia"];

  hardware.nvidia = {
    # modesetting.enable = true;
    # powerManagement.enable = false;
    # powerManagement.finegrained = false;
    open = false;
    nvidiaSettings = true;
    package = config.boot.kernelPackages.nvidiaPackages.stable;
  };

  networking.hostName = "quote";
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Configure keymap in X11
  services.xserver = {
    xkb.layout = "us, br";
    xkb.variant = "colemak_dh_ortho,";
    xkb.options = "grp:menu_toggle";
  };

  # Home-manager module
  home-manager = {
    useGlobalPkgs = true;
    extraSpecialArgs = {inherit inputs;};
    users = {
      "paranoia-agent" = import ../../homes/quote/home.nix;
    };
  };

  system.stateVersion = "24.05";
}
